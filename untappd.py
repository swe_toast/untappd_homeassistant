"""
A component which allows you to get information from Untapped.
For more details about this component, please refer to the documentation at
https://gitlab.com/swe_toast/untappd_homeassistant
"""
import voluptuous as vol
from datetime import timedelta
from homeassistant.helpers.entity import Entity
import homeassistant.helpers.config_validation as cv
from homeassistant.components.switch import (PLATFORM_SCHEMA)

REQUIREMENTS = ['pyuntapped==0.0.1']

CONF_USERNAME = 'username'
CONF_ID = 'id'
CONF_SECRET = 'secret'

ATTR_BEER = 'beer'
ATTR_SCORE = 'score'
ATTR_COMPONENT = 'component'
ATTR_COMPONENT_VERSION = 'component_version'

SCAN_INTERVAL = timedelta(seconds=120)

ICON = 'mdi:beer'
COMPONENT_NAME = 'untapped'
COMPONENT_VERSION = '0.0.1'

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_USERNAME): cv.string,
    vol.Required(CONF_ID): cv.string,
    vol.Required(CONF_SECRET): cv.string,
})

def setup_platform(hass, config, add_devices, discovery_info=None):
    username = config.get(CONF_USERNAME)
    api_id = config.get(CONF_ID)
    api_secret = config.get(CONF_SECRET)
    add_devices([UntappedSensor(username, api_id, api_secret)])

class UntappedSensor(Entity):
    def __init__(self, username, api_id, api_secret):
        from pyuntapped import Untapped
        self._untapped = Untapped()
        self._username = username
        self._apiid = api_id
        self._apisecret = api_secret
        self._component = COMPONENT_NAME
        self._componentversion = COMPONENT_VERSION  
        self._state = None
        self.update()


    def update(self):
        result = self._untapped.getLastActivity(self._apiid, 
            self._apisecret, self._username)
        if not result :
            return False
        else:
            self._state = result['created_at']
            self._beer = result['beer']['beer_name']
            self._score = str(result['rating_score'])
    
    @property
    def name(self):
        return 'Untapped Last Check-in'

    @property
    def state(self):
        return self._state

    @property
    def icon(self):
        return ICON

    @property
    def device_state_attributes(self):
        return {
            ATTR_BEER: self._beer,
            ATTR_SCORE: self._score,
            ATTR_COMPONENT: self._component,
            ATTR_COMPONENT_VERSION: self._componentversion
        }